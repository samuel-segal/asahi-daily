from bs4 import BeautifulSoup
import codecs
import os

#Triple r!
import re
import requests
import random


def getRandomArticle():
    website = requests.get('https://www.asahi.com/')
    soup = BeautifulSoup(website.content, 'html.parser')
    hrefList = soup.find_all('a',href=True);


    catList = []
    for element in hrefList:
        href = element['href']
        if href.find('/list') != -1:
            catList.append(href)


    while len(catList) > 0:
        index = random.randrange(len(catList))
        category = catList.pop(index)
        article = articleFromCategory(category)
        if article:
            return article


urlFinder = re.compile(r'/articles/.+')
urlImprover = re.compile(r'^/?articles')
def articleFromCategory(category,limit=3):
    catWeb = requests.get(category)
    catSoup = BeautifulSoup(catWeb.content, 'html.parser')
    artUrls = catSoup.find_all('a',href=urlFinder,limit=limit)
    while len(artUrls) > 0:
        index = random.randrange(len(artUrls))
        href = artUrls.pop(index)['href']
        url = href
        if urlImprover.match(url):
            url = 'https://www.asahi.com'+url
        print(url)
        article = getArticle(url)
        if article:
            return article
        
    return None

def getArticle(articleUrl):
    artWeb = requests.get(articleUrl)
    artSoup = BeautifulSoup(artWeb.content, 'html.parser')
    text = artSoup.find('div',class_='ArticleText')
    children= text.findChildren('p')
    output = ''
    for child in children:
        output += child.text+'\r\n'
    return output

article = getRandomArticle()
print(article)

file = codecs.open('news.txt','w','utf-8')
file.write(article)
file.close()

os.startfile('news.txt','print')
